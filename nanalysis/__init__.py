from .spectrometer import NMR
from .version import __version__

__all__ = [
    'NMR',
]
