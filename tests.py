import unittest
from nanalysis.spectrometer import NMR, BadResponseCode

# known (accessible) address of the spectrometer on the network
address = '10.10.3.50'


class TestNMR(unittest.TestCase):
    def setUp(self):
        self.nmr = NMR(address)
        if self.nmr.rpc_enabled is False:
            raise ConnectionError('The spectrometer must have RPC enabled for the test suite to run')

    def test_connection(self):
        self.assertEqual(self.nmr.ping(), True)
        self.assertEqual(self.nmr.rpc_enabled, True)
        with self.assertRaises(ConnectionError):
            self.nmr.address = '10.10.1.100'
        self.assertEqual(self.nmr.address, address)

    def test_get_and_put(self):
        # test get
        self.assertEqual(
            self.nmr._api_get('/interfaces/iStatus/PingSpectrometer'),
            {'connected': True}
        )
        # test put
        self.assertEqual(
            self.nmr._api_put(
                '/interfaces/iFlow/CancelExperiment',
                {}
            ),
            {"ResultCode": 0}
        )

        # todo test put and parse

    def test_experiment_settings(self):
        """Tests retrieval and modification of experiment settings"""
        original = self.nmr.general_experiment_settings

        # grab current and modify some settings
        modified = self.nmr.general_experiment_settings
        modified['Apodization'] += 0.1
        modified['Experiment'] = 2
        modified['NumberOfPoints'] *= 2
        modified['NumberOfScans'] = 3

        self.nmr.change_general_settings(**modified)

        self.nmr.change_general_settings(**original)

    def test_manual_integrals(self):
        """Tests addition and removal of manual integrals"""
        original = self.nmr.reset_manual_integrals()
        self.nmr.add_manual_integral(1., 1.5)
        self.nmr.add_manual_integral(2.2, 3.)
        self.nmr.add_manual_integral(7, 7.5)
        self.assertEqual(
            self.nmr.manual_integrals,
            {'Integrals':
                [
                    {'RegionEnd': 1.5, 'RegionStart': 1.0},
                    {'RegionEnd': 3.0, 'RegionStart': 2.2},
                    {'RegionEnd': 7.5, 'RegionStart': 7.0}
                ],
                'ReferenceEnergy': 1.0
            }
        )
        # reset to original
        self.nmr.manual_integrals = original
