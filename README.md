# Nanalysis API python wrapper

## Installation

This package is not yet registered on PyPI, so manual installation is required. 
Download and extract the whole package from Gitlab and extract to a desired folder. 
This package requires only the `requests` nonstandard library. 

## Connecting to a spectrometer

RPC must be physically enabled on the spectrometer. Click `Setup`, 
then the `Remote` tab, then select `Enable RPC`. If this is not enabled, 
only property accession will be possible (no setting or experiment initiation). 
You will also need the network IP address of the spectrometer. This is shown 
in the `Network` tab of the `Setup` section. 

To connect to the spectrometer, import the NMR class and instantiate by 
passing the IP address as a string as the first argument. 

```python
>>> from nanalysis import NMR
>>> nmr = NMR('10.10.1.50')  # your physical address goes here
NMR(10.10.1.50)
```

## NMR status and utilities

### Status checks
During instantiation, the spectrometer is pinged and a check is performed 
to verify that RPC is enabled. An error or warning is issued respectively 
if either of these fails. These can be manually checked using the `ping()`
method and `rpc_enabled` property. 

#### `ping()`
Verifies that Python can communicate with the spectrometer over the network. 
```python
>>> nmr.ping()  # returns True if the spectrometer is reached
True
```

#### `rpc_enabled`
Checks the RPC setting of the spectrometer (remote calls). 
```python
>>> nmr.rpc_enabled  # returns True if RPC is enabled
True
```

#### `startup_test_status`
Returns the startup status of the spectrometer. 
```python
>>> nmr.startup_test_status
{'Message': 'Finished', 'PercentComplete': 102, 'ResultCode': 0}
```

#### `experiment_status`
Returns detailed output of the status of the current experiment. 
```python
>>> nmr.experiment_status
{
    'IntegralReport': {}, 
    'JDX_FileContents_FD': '', 
    'JDX_FileContents_TD': '', 
    'JDX_Filename': '', 
    'NumberOfScansRun': 0, 
    'OriginalReceipt': {}, 
    'PeakList': [1.2865746019953197, 2.710902151914888, 7.289097848085111], 
    'PeakThresholdValue': 1.9214895963668823, 
    'ResultCode': 0
}
```
Additional properties are provided to access some of these values directly: 
- `last_experiment_jcamp` returns the FID of the last experiment in JCAMP-DX format
- `last_experiment_filename` returns the file name for the JCAMP-DX of the last experiment
- `last_experiment_integrals` returns the details (`dict`) of the integrals of the last experiment
- `last_experiment_peaks` returns a list of peaks from the last experiment
- `last_experiment_timestamp` returns the timestamp of the last experiment run
- `last_experiment_settings` returns the experiment settings from the last experiment run

#### `standby`
This property allows querying or setting of standby mode for the spectrometer. 

```python
>>> nmr.standby
False
>>> nmr.standby = True  # turns standby mode on
>>> nmr.standby = False  # turns standby mode off
```


## Experiment settings

Experiment settings are exposed under both the `all_experiment_settings` 
property (returns a dictionary of settings and values) and as `NMR` properties 
with each property exposing the value (bypassing dictionary parsing). 
Experiment settings can be modified by these methods: 

1. Setting `all_experiment_settings` by passing a dictionary containing 
   `all_experiment_settings` keys and values. 
2. Calling `change_experiment_settings` using the same syntax as (1). 
3. Setting the `NMR` object properties with values directly. This method is 
   recommended for the greatest degree of feedback for erronious settings. 
   There are also additional type and value checks built into the property setters. 

### Keys, property names, and descriptions

| `all_experiment_settings` key | `NMR` class property |Setting Supported| Brief description |
|-------------------------------|----------------|---|--------------------|
| ActiveTimeScanInSeconds       | `active_time_scan` || The time during which the spectrometer is busy acquiring and processing data. |
| Apodization | `apodization` | X| |
| DigitalResolutionInHz | `digital_resolution`| |Digital resolution of the spectrometer |
| Experiment | `experiment` | X |The type of experiment being run. If accessed via the dictionary, this is an integer corresponding to the experiment index of 'ExperimentNameAvailable'. The `experiment` property returns the experiment name, and can be set as either an integer or valid experiment name. |
| ExperimentName| `experiment` | |The name of the selected experiment. |
| ExperimentNameAvailable | `experiments` | |The list of available experiments |
| LockNucleus |  | |The lock nucleus. This is always '2H'. |
| LockNucleusAvailable |  | |available lock nuclei |
| Nucleus | `nucleus` |X |The target nucleus. |
| NucleusAvailable | `nuclei` | | The available nuclei |
| NumberOfPoints | `number_of_points` | X | Number of data points acquired by the spectrometer for each scan|
| NumberOfScans | `number_of_scans` | X | Number of scans in an experiment | 
| PeakIntegrationMethod | `peak_integration_method` | X| How the spectrometer integrates. 0: manual, 1: automatic |
| PulseWidthInMicroseconds| `pulse_width` || Duration of the 90 degree pulse |
| ReceiverGain |`receiver_gain` | X | Gain setting for the receiver |
| ScanDelayInSeconds | `scan_delay` | X| Scan delay; allows for sample relaxation between scans | 
| Solvent | `solvent` | X | The solvent being used. If accessed via the dictionary, this is an integer corresponding to the experiment index of 'Solvents'. The `solvent` property returns the solvent name, and can be set as either an integer or valid solvent name. |
| SolventGroup | `solvent_group` | X | Effectively identical to `nucleus` |
| | `solvents` | | The available nuclei and solvents. This is exposed as a dedicated API call and does not have a key in `all_experiment_settings`. 
| SpectralCenterInPpm | `spectral_center` | X | The center of the scanned area in ppm. |
| SpectralWidthInPpm | `spectral_width` | X | The span of the scanned area in ppm. |
| TimePerScanInSeconds | `time_per_scan` | | The time taken to complete a single scan. |
| TotalDurationInSeconds | `total_duration` | | The estimated time the entire experiment will take to run. |
| ZeroFillingFactor | `zero_filling` | X| Zero filling factor to enhance digital resolution. |


## Shimming and running experiments

### Shimming

A shim can be initiated using the `shim()` method. By default, a quick 
auto-shim is performed, but this can be changed by passing `2` (medium auto-shim) 
or `3` (full auto-shim) as the first argument (e.g. `shim(2)` or `shim(method=2)`). 

One can wait for the shim to complete by either setting `wait=True` in the `shim()` call 
(`shim(wait=True)`) or by calling the `wait_for_shim()` method. 

The shim can be cancelled by calling the `cancel_shim()` method. 

### Running an experiment

An experiment can be initiated using the `run_experiment()` method. 
Keyword arguments may be passed corresponding to experiment setting keys to 
modify experiment settings prior to executing the experiment, but for the greatest 
degree of control it is recommended to modify settings before the `run_experiment()` call. 

`wait=True` may be passed to the `run_experiment()` call to wait for the 
experiment to complete before continuting. Alternatively, the `wait_for_experiment()` 
method can be called. The experiment is complete when the `'ResultCode'` 
key of `experiment_status` is equal to `0`. 

The experiment can be cancelled using the `cancel_experiment()` method. 

## Integrals and Peak detection

For auto-integration, the threshold for peak detection can be accessed and 
modified via the `peak_threshold_multiplier` property. The relationship between 
the threshold multiplier and the threshold value is ThresholdValue = ThresholdMultiplier * NoiseLevel. 

Manual integrals may be viewed or set using the `manual_integrals` property, 
but the syntax of the dictionary is finicky, so an `add_manual_integral` method 
was written to simplify integral definition. 

To add an integral, provide the start and end ppm values to this method. 

```python
>>> nmr.manual_integrals  # current state of the manual integrals
{'Integrals': [], 'ReferenceEnergy': 3506.008719592004}
>>> nmr.add_manual_integral(1., 1.5)  # will integrate between 1 and 1.5 ppm
>>> nmr.manual_integrals
{'Integrals': [{'RegionEnd': 1.5, 'RegionStart': 1.0}], 'ReferenceEnergy': 3506.008719592004}
```

There is currently no method to remove individual integrals, but all integrals 
may be reset using the `reset_manual_integrals()` method. This method returns 
the current state of the integrals as defined on the spectrometer. 

```python
>>> nmr.reset_manual_integrals()
{'Integrals': [{'RegionEnd': 1.5, 'RegionStart': 1.0}], 'ReferenceEnergy': 3506.008719592004}
>>> nmr.manual_integrals
{'Integrals': [], 'ReferenceEnergy': 1.0}  
```

## Developer Notes

For implementing new API functionality, the API get and put methods are 
`_api_get(path)` and `_api_put_(path)` respectively, where `path` is the 
API path. For example, the API get from `http://10.10.1.50:5000/interfaces/iStatus/PingSpectrometer` 
is accomplished by  

```python
# the nmr address is set on instantiation and the protocol and port are attributes
>>> nmr._api_get('/interfaces/iStatus/PingSpectrometer')
{'connected': True}
```

Putting is accomplished by providing a path and a JSON-like object formatted 
as described in the API. For example, entering standby mode is accomplished 
by 
```python
>>> nmr._api_put('/interfaces/iStatus/StandbyMode', {'StandbyMode': True})
{'ResultCode': 0}
```

The returned dictionary is typically difficut to interpret without context, 
so an additional abstraction layer for putting is provided by `_api_put_and_parse()` 
which takes an additional dictionary of response codes and their meaning. 
This method is helpful for automatically parsing the return JSON object from 
putting in the context of the API path. The default value has a result code of 
0 as a success and 1 as a failure. The return of the method is `True` for 
a successful put and parsing, and a `BadResponseCode` error with context-specific 
interpretation is raised otherwise. The previous example would then become
```python
>>> nmr._api_put_and_parse('/interfaces/iStatus/StandbyMode', {'StandbyMode': True})
True
```

While most API puts only have result codes of 0 and 1, more complicated 
puts like `RunExperiment` have more possible responses
```python
{  # possible response codes for running an experiment
    0: 'Succeeded, experiment started',
    1: 'Failed due to active auto-shimming session',
    2: 'An experiment is already running',
    3: 'No response',
    4: 'Bad parameters',
    5: 'No such experiment',
}
```