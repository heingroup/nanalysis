import nanalysis.version
from setuptools import setup, find_packages

NAME = 'nanalysis'
AUTHOR = 'Lars Yunker / Hein Group'

PACKAGES = find_packages()
# KEYWORDS = ', '.join([
# ])

# with open('LICENSE') as f:
#     lic = f.read()
#     lic.replace('\n', ' ')

# with open('README.MD') as f:
#     long_description = f.read()

setup(
    name=NAME,
    version=nanalysis.version.__version__,
    description='API python wrapper for Nanalysis NMReady spectrometers',
    # long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    url='https://gitlab.com/heingroup/nanalysis',
    packages=PACKAGES,
    # license=lic,
    python_requires='>=3.6',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Chemistry',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Operating System :: Microsoft :: Windows',
        'Natural Language :: English'
    ],
    setup_requires=[
        'requests>=2.21.0',
        'tqdm>=4.41.1',
        'numpy>=1.17'
    ],
    install_requires=[
        'requests>=2.21.0',
        'tqdm>=4.41.1',
        'numpy>=1.17'
    ]
)
