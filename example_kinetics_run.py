"""
A simple kinetics experiment which will run an NMR experiment a specified number of times at the time spacing specified.

The loop is structured to allow modification of the number of experiments or spacing on-the-fly (the user can break
out of the loop, redefine the values, and re-exectue the loop without issue.
"""

import time
from nanalysis import NMR


def sleep_until_time(until, poll=0.1):
    """
    Method to sleep until the provided time.

    :param float until: time since epoch to wait until
    :param float poll: poll frequency to check condition
    """
    if until < time.time():  # if the time has already passed
        return
    print(f'Waiting ~{until - time.time():.1f}s until next timepoint. ')
    while time.time() < until:
        time.sleep(poll)


# connect to the spectrometer and perform shim
nmr = NMR('10.10.1.50')
nmr.shim()

nmr.wait_for_shim()  # wait for the shim to complete

# number of timepoints to take
number_of_timepoints = 10

# spacing between experiments (seconds)
time_spacing = 1 * 60.

# timepoint storage variable (don't modify this)
_LAST_TIMEPOINT = 0.
# experiment counter (don't modify this)
_EXPERIMENT_COUNTER = 0

# loop while the number of timepoints is less than the total
while _EXPERIMENT_COUNTER < number_of_timepoints:
    sleep_until_time(_LAST_TIMEPOINT + time_spacing)  # wait until next timepoint
    nmr.wait_for_experiment()  # ensure previous experiment is complete
    nmr.run_experiment()  # run experiment
    _LAST_TIMEPOINT = time.time()  # start time for experiment
    _EXPERIMENT_COUNTER += 1  # increase experiment counter
    print(f'Running experiment #{_EXPERIMENT_COUNTER}/{number_of_timepoints} ({nmr.experiment_status["JDX_Filename"]}).')

